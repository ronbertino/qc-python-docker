This Ubuntu 18.04 Docker container installs Quantconnect Lean and all Python v.3.6.8 dependencies and configures Lean to use Python by default.  The working directory is /usr/local/src/Lean.  

HOW TO USE:

Install Docker (assumes a Ubuntu Linux system):
sudo apt install docker.io

Start Docker:
sudo systemctl enable --now docker

Create Quantconnect LEAN Docker image:
1) Navigate to the location where the qc-python-docker/Dockerfile lives.
2) sudo docker build -t qc-python-docker ./
3) Go for a run, bake some sourdough loaves, and have a few drinks-- this step takes a good long while. 
4) Working directory is /usr/local/src/Lean/

Run the Docker Image:
sudo docker run --rm --interactive --tty qc-python-docker:latest /bin/bash

List Docker Images:
sudo docker image list

How to Delete the Docker Image:
sudo docker rmi --force qc-python-docker qc-python-docker

If you don't want to run Docker as sudo, consult this webpage:
https://docs.docker.com/engine/install/linux-postinstall/

If you want to develop with Microsoft VS Code from your local environment, you can build the container *inside* VS Code.
https://code.visualstudio.com/docs/remote/containers (I have not attempted as of 8/25/20.)

